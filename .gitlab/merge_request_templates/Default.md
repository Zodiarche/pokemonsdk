Thank you for your contribution to the **PSDK** repo.

Before submitting this Merge Request into the development branch, please make sure:
- [ ] Your code runs clean without any errors or warnings
- [ ] You are following the [Contributing guidelines](../development/CONTRIBUTING.md)
- [ ] You tested your code to make sure it does what it is supposed to do

### MR Description
- Description

### Before testing
- Thing to do

### Notes to acknowledge before testing
- Thing to acknowledge

### Tests to realize
- [ ] First test

<!-- if appliable !-->
### Showcasing content
<!-- [image showcasing](url_image.png) !-->
<!-- [video showcasing](url_video.mp4) !-->

<!-- if appliable !-->
### Sources related to this MR
- [Bulbapedia link](https://bulbapedia.bulbagarden.net)
- [Pokepedia link](https://www.pokepedia.fr)
- [Discord link of the support](https://ptb.discord.com)
- [Discord link of the dev topic](https://ptb.discord.com)
- [ClickUp link of the ticket](https://app.clickup.com)
